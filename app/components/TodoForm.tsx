import { Form, useNavigation } from "@remix-run/react"

import styles from "~/styles/TodoForm.css"

function TodoForm(){
    const navigation = useNavigation();
    const isSubmitting = navigation.state === 'submitting';

    return(
        <div id="form-container">
            <Form method="post" id="todo-form">
                <input type="hidden" name="_action" value="create"></input>

                <p id="topic">
                    <label htmlFor="topic">Topic</label>
                    <input type="text" name="topic" required/>
                </p>
                <p id="description">
                    <label htmlFor="description">Description</label>
                    <textarea name="description" rows={3} required/>
                </p>
                <div className="form-actions">
                    <button name="_intent" value="create" disabled={isSubmitting}>
                        Add Todo
                    </button>
                </div>

            </Form>
        </div>
    )
}

export default TodoForm

export function links(){
    return [{rel: 'stylesheet', href:styles}]
}