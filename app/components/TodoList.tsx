
import {TodoObject} from "~/store/todo-data";
import React, { useState } from 'react';


import styles from "~/styles/TodoList.css"

import { Form, useNavigation } from "@remix-run/react"


function TodoList({ todoList }: { todoList: TodoObject[] }){

    const navigation = useNavigation();
    const isSubmitting = navigation.state === 'submitting';

    const [activeTab, setActiveTab] = useState("0");

    const handleOptionChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const newValue = event.target.value;
        setActiveTab(newValue);
      };

    return(
        <div id="list-container">
            <div id="select-option">
                <select value={activeTab} onChange={handleOptionChange}>
                    <option value="0">Pending</option>
                    <option value="1">Completed</option>
                    <option value="2">All</option>
                </select>
            </div>
             <ul id="todo-list">
                {todoList.map((todo, idx) => (
                   <>
                   { (activeTab == "2" || (activeTab == "0" && todo.status == "pending") || (activeTab == "1" && todo.status == "completed"))?
                
                    <li id={todo.status == "completed" ?"list-completed":"list-pending"}>
                        <div id="todo-id">
                            <span>No: {idx+1}</span>
                            <label>{todo.id}</label>
                        </div>
                        <div id="todo-topic">
                            <label>{todo.topic}</label>
                            {/* <span> {todo.status == "completed? "Completed":"Pending"}</span> */}
                        </div>

                        <p>
                            {todo.description}
                        </p>
                        
                        <Form method="post" id="todo-update-form">
                            {/* <input type="hidden" name="_action" value="update_status"></input> */}
                            <input type="hidden" name="id" value={todo.id}></input>
                            <input type="hidden" name="status" value={todo.status == "completed"? "pending":"completed"}></input>
                        {todo.status == "completed" ?
                            <button name="_intent" value="update_status"  disabled={isSubmitting} id="button-pending">Move to pending</button>:
                            <button name="_intent" value="update_status"  disabled={isSubmitting} id="button-complete">Mark as completd</button>
                        }
                        </Form>
                    </li>:<></>
                    }
                    </>
                    
                ))
                }
            </ul>
        </div>
       
    )
}

export default TodoList;

export function links(){
    return [{rel: 'stylesheet', href:styles}]
}
