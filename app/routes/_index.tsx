import { Link } from '@remix-run/react';

import indexStyle from "~/styles/index.css"

export default function Index() {
  return (
    <main id="content">
        <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.8" }}>
          <h1>Welcome to Todo</h1>
          <Link to="/todo">Enter</Link>
        </div>
    </main>
  );
}

export function links() {
  return [{ rel: 'stylesheet', href: indexStyle  }];
}