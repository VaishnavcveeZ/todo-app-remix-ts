
import TodoForm, {links as todoFormLink} from "~/components/TodoForm";
import TodoList, {links as todoListLink} from "~/components/TodoList";

import {getAllTodos} from "~/store/todo-data"
import { storeTodos } from "~/store/todo-data";
import { updateData } from "~/store/todo-data";

import TodoStyle from "~/styles/todo.css"

import {useLoaderData} from "@remix-run/react"

import type {
    ActionFunctionArgs,
  } from "@remix-run/node";

import { json, redirect } from "@remix-run/node";

export default function Index() {

  const data = useLoaderData<typeof loader>();

    return (
        <main id="content">
            <div>
                <h1>TODO</h1>
                <TodoForm/>
                <h2>Your todo list</h2>
                <TodoList todoList={data.todos}/>
            </div>
      </main>
    );
  }

  export function links() {
    return [{ rel: 'stylesheet', href: TodoStyle}, ...todoFormLink(), ...todoListLink() ];
  }

  export const action = async ({
    params,
    request,
  }: ActionFunctionArgs) => { 

    const formData = await request.formData();
    const intent = formData.get("_intent") as string // get button _intent

    const todoData = JSON.stringify(Object.fromEntries(formData));
 
    switch(intent) { 
      case "create": { 
        storeTodos(todoData)
        break; 
      }
      case "update_status": { 
        updateData(todoData) 
        break; 
     } 
      default: {  
         break; 
      } 
   } 
    
    if (intent == "create" ){
    } else {
    }
  
    return redirect(`/todo`);
  };

  export async function loader() {
    const todos = await getAllTodos()

    return todos
  }