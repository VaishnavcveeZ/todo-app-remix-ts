import fs from 'fs/promises';

export interface TodoObject {
  id: string;
  topic: string;
  description: string;
  status: string;
}

interface TodosData {
  todos: TodoObject[]
}

export async function getAllTodos():Promise<TodosData> {
  const rawFileContent = await fs.readFile('todos.json', { encoding: 'utf-8' });
  const data:TodosData = JSON.parse(rawFileContent);
   if (!(data?.todos)) data.todos = []
  return data;
}

export function storeTodos(todoInput: any) {

  console.log(todoInput)

  const newTodo: TodoObject = JSON.parse(todoInput) 
  newTodo.id = new Date().toISOString();
  newTodo.status = "pending"

  getAllTodos()
  .then((todosData)=>{
    todosData.todos.push(newTodo)
    fs.writeFile('todos.json', JSON.stringify(todosData)); 

  })
  .catch((err)=>{
    console.log(err)
  })
 
  return 
}

export interface TodoUpdateStatusObject {
  id: string;
  status: string;
}
export function updateData(todoInput: any){

  const updateTodo: TodoUpdateStatusObject = JSON.parse(todoInput) 

  getAllTodos()
  .then((todosData)=>{

    todosData.todos.forEach(item => {
      if (item.id == updateTodo.id){
        item.status = updateTodo.status

        Object.assign(item, item);
      }
    })

    fs.writeFile('todos.json', JSON.stringify(todosData)); 

  })
  .catch((err)=>{
    console.log("err",err)
  })

}